#include <stdio.h>
#include <bitmap.h>
#include <debug.h>
#include <limits.h>
#include <round.h>
#include <list.h>
#include "devices/block.h"
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "threads/pte.h"
#include "threads/palloc.h"
#include "vm/swap.h"
#include "vm/page.h"
#include "filesys/cache.h"

typedef unsigned long elem_type;

struct lock swap_lock;

struct bitmap
{
  size_t bit_cnt;
  elem_type *bits;
};

struct bitmap * swap_space;
struct block * swap_block;

#define ELEM_BITS (sizeof (elem_type) * CHAR_BIT)

static inline size_t
elem_cnt (size_t bit_cnt)
{
return DIV_ROUND_UP (bit_cnt, ELEM_BITS);
}

/* Returns the number of bytes required for BIT_CNT bits. */
static inline size_t
byte_cnt (size_t bit_cnt)
{
  return sizeof (elem_type) * elem_cnt (bit_cnt);
}

bool
swap_init (void)
{
  swap_block = block_get_role (BLOCK_SWAP);

  int pgcnt = block_size (swap_block) / PGSIZE;
  if (block_size (swap_block) % PGSIZE)
    pgcnt++;

  swap_printf("swap size %d sectors\n", block_size (swap_block));

  void * base = palloc_get_multiple(0, pgcnt);
  swap_space = bitmap_create_in_buf (block_size (swap_block), base, pgcnt*PGSIZE);
  lock_init (&swap_lock);

  return true;
}

/* find a contiguous 8 sector block in swap space and write the
  data pointed by P to the swap space. Mark the struct page * P
  as PAGE_in_swap and record the sector_id of the swap space used */
bool
swap_out (struct page * p)
{
  lock_acquire (&swap_lock);
  p->flags |= PAGE_in_swap;
  uint32_t * pte = p->pte;
  /* mark hardware PT so the next time this thread accesses this
    page, it needs to fetch it from swap */
  *pte &= ~PTE_P;
  *pte &= ~PTE_A;
  *pte &= ~PTE_D;

  struct frame * f = p->fs;
  if (f==NULL)
  {
    printf("frame NULL during swap_out\n");
    lock_release (&swap_lock);
    return false;
  }
  size_t idx = bitmap_scan_and_flip (swap_space, 0, SEC_CNT_PAGE, false);
  if (idx==BITMAP_ERROR)
  {
    printf("bitmap error\n");
    lock_release (&swap_lock);
    return false;
  }
  
  p->sector = idx;
  p->fs = NULL;
  
  lock_acquire (&f->flock);
  int i;
  for (i=0; i<SEC_CNT_PAGE; i++)
    block_write (swap_block, idx + i, (void *)f->paddr + i*BLOCK_SECTOR_SIZE);
  /* page-sharing:
    find all pages linking to this frame and sest their p->fs to NULL */
  struct list_elem * e;
  struct page * dum_page;
  for ( e = list_begin (&f->page_list); e != list_end (&f->page_list); e = list_next (e))
  {
    dum_page = list_entry (e, struct page, frame_list_elem);
    dum_page->fs = NULL;
    pte = dum_page->pte;
    *pte &= ~PTE_P;
    *pte &= ~PTE_A;
    *pte &= ~PTE_D;
  }
  lock_release (&f->flock);
  
  if (p == NULL)
  {
    printf("page struct is NULL during swap_out\n");
    lock_release (&swap_lock);
    return false;
  }
  //printf("t(%x) pte(%x) = %x\n", thread_current(), p->pte, *(p->pte));
  palloc_free_page (pte_get_page(*(p->pte)));
  lock_release (&swap_lock);
  return true;
}

bool
swap_in (struct page * p, void * paddr)
{
  lock_acquire (&swap_lock);
  p->flags &= ~PAGE_LOCS;
  uint32_t * pte = p->pte;
  *pte |= PTE_P;

  ASSERT( p->sector >= 0);

  int i;
  for (i=0; i<SEC_CNT_PAGE; i++)
    block_read (swap_block, p->sector + i, (void *)paddr + i * BLOCK_SECTOR_SIZE);

  bitmap_set_multiple (swap_space, p->sector, SEC_CNT_PAGE, false);
  lock_release (&swap_lock);
  return true;
}

bool
swap_thread_destroy (struct thread * t)
{
  
}
