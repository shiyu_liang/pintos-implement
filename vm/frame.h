#ifndef FRAME_H
#define FRAME_H

#include <list.h>
#include <stdio.h>
#include "threads/thread.h"
#include "threads/synch.h"
#include "devices/block.h"
#include "filesys/off_t.h"

#define frame_printf(...) printf("frame: " __VA_ARGS__)

/* use linked-list for hash table because "clock" algorithm
  operates on queues */
struct frame {
  void * vaddr;           /* pointer to page currently occupying frame */
  void * paddr;           /* pointer to actual frame */
  uint32_t * pte;
  struct thread * owner;
  struct page * page;
  struct list page_list;  /* list of page structures ref this frame */
  struct list_elem frame_elem;  /* to insert into global framlist */
  struct list_elem shared_frame_elem;
  int shared_cnt;
  struct lock flock;
};

bool frame_init (void);
struct frame * init_frame (void);
bool frame_insert (struct frame*);
bool frame_remove (struct frame*);
bool frame_evict(void);
bool frame_evict_int(int);
void * frame_get_page(enum palloc_flags);
bool frame_thread_destroy (struct thread *);
void frame_dump (struct frame *);
struct frame * frame_get_shared (void *);

#endif
