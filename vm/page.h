#ifndef PAGE_H
#define PAGE_H

#include <hash.h>
#include <inttypes.h>
#include "vm/frame.h"
#include "filesys/off_t.h"
#include "devices/block.h"
#include "threads/thread.h"
#include "threads/synch.h"

// mutually exclusive
// these bits tell where the page is
#define PAGE_LOCS       0x7
#define PAGE_in_mem     0x1
#define PAGE_in_fs      0x2
#define PAGE_in_swap    0x4

// mutually exclusive
// these bits tell what the page represents
#define PAGE_TYPES      0x70
#define PAGE_is_stack   0x10
#define PAGE_is_code    0x20
#define PAGE_is_mmap    0x40

#define PAGE_all_zero   0x100
#define PAGE_unmmapped  0x200

#define SEC_CNT_PAGE (PGSIZE/BLOCK_SECTOR_SIZE)    // number of sectors in a page

// these bits are set the first time they are brought in

#define page_printf(...) printf("page: " __VA_ARGS__)

struct lock glb_swap_lock;
struct lock glb_fs_lock;

struct page {
  struct hash_elem hash_elem;
  struct list_elem list_elem;
  struct list_elem mmap_list_elem;
  struct list_elem frame_list_elem; // for page-sharing
  void * mmap_addr;
  /* flags tell whether the page is in filesystem or swap
    or all-zero page. Tell whether the page is shared and loaded
    in some frame, but not mapped to the page.*/
  unsigned long flags;
  struct thread * owner;
  /* using pointers to PTEs have few advantages. Firstly these
  pointers are unique to a process. Two threads accessing the same
  virtual page derive these pages from two separate PTEs. This means
  they can also be used in hash functions. Second,
  PTEs can be used to find the virtual addresses easily */
  uint32_t * pte;
  struct frame * fs;
  struct inode * inode;
  struct lock plock;
  off_t offset;
  size_t read_bytes;
  bool writable;
  int usr_cnt;  /* spawning off child process may share the same PTs,
    because when a thread is newly created, init_page_dir is copied */
  block_sector_t sector;
};

bool page_init(void);
struct page * init_page(void);
struct page * page_lookup (const void *);
bool page_save (struct page *);
bool page_set_frame (struct page *, struct frame *);
uint32_t page_get_pte (uint32_t * pd, void * vaddr, bool create);
bool page_get_data (struct page *);
bool page_thread_destroy (struct thread *);
bool page_destroy (struct page *);
bool page_set_up (struct page*, struct thread *, uint32_t *,
    uint32_t, uint32_t, void *);

#endif
