#ifndef SWAP_H
#define SWAP_H

#include "threads/thread.h"
#include "vm/page.h"

#define swap_printf(...) printf("swap: " __VA_ARGS__)

struct swap_entry {
  struct hash_elem * elem;
};

bool swap_init(void);
bool swap_out (struct page *);
bool swap_in (struct page *, void *);
bool swap_thread_destroy (struct thread *);

#endif
