#include <list.h>
#include "threads/pte.h"
#include "threads/synch.h"
#include "threads/palloc.h"
#include "threads/malloc.h"
#include "vm/frame.h"
#include "vm/page.h"

struct list frames;
struct list shared_frames;
struct lock frame_lock;
struct list_elem * g_frame_ptr;

bool
frame_init (void)
{
  frame_printf("initializing frame list\n");
  list_init(&frames);
  list_init(&shared_frames);
  lock_init(&frame_lock);
  return true;
}

struct frame *
init_frame (void)
{
  struct frame * f  = malloc (sizeof (struct frame)); 
  f->shared_cnt = 0;
  list_init (&f->page_list);
  lock_init (&f->flock);
  return f;
}

/* insert struct frame into global list of frames */
bool
frame_insert (struct frame * f)
{
  ASSERT (f != NULL);
  lock_acquire (&frame_lock);
  list_push_back (&frames, &f->frame_elem);
  lock_release (&frame_lock);
  return true;
}

bool
frame_remove (struct frame * f)
{
  ASSERT (f != NULL);
  lock_acquire (&frame_lock);
  list_remove (&f->frame_elem);
  lock_release (&frame_lock);
  return true;
}

bool
frame_evict(void)
{
  if (list_size(&frames) <= 0)
  {
    return false;
  }
  struct list_elem * e;

  if ( g_frame_ptr == NULL )
    e = list_begin(&frames);
  else
    e = g_frame_ptr;

  uint32_t * pte;
  struct page * p;
  while (true)
  {
    struct frame * f = list_entry (e, struct frame, frame_elem);
    p = f->page;
    if (p == NULL)
    {
      f->page = NULL;
      goto advance;
    }
    pte = p->pte;
    if (pte==NULL)
    {
      return false;
    }
    void * dum = *pte & PTE_A;
    if (dum)
    {
      *pte &= ~PTE_A;
    }
    else
    {
       lock_acquire (&glb_swap_lock);
       lock_acquire (&p->plock);
       swap_out(p);
       lock_release (&p->plock);
       lock_release (&glb_swap_lock);
       list_remove (e);
       g_frame_ptr = list_next(e);
       if (g_frame_ptr==list_end(&frames))
         g_frame_ptr = list_begin(&frames);
       free(f);
       return true;
    }
advance:
    e = list_next(e);
    if (e==list_end(&frames))
      e = list_begin(&frames);
  }
  return true;
}

/* evict CNT frames */
bool
frame_evict_int(int cnt)
{
  if (list_size(&frames) <= 0)
  {
    return false;
  }
  struct list_elem * e = list_begin(&frames);
  uint32_t * pte;
  struct page * p;
  int i;
  bool flipped = false;
  for (i=0; i<cnt && e != list_end(&frames); i)
  {
    struct frame * f = list_entry (e, struct frame, frame_elem);
    p = f->page;
    if (p == NULL)
    {
      f->page = NULL;
      continue;
    }
    pte = p->pte;
    if (pte==NULL)
    {
      return false;
    }
    void * dum = *pte & PTE_A;
    if (dum)
    {
      *pte &= ~PTE_A;
      flipped = true;
    }
    else
    {
       lock_acquire (&glb_swap_lock);
       lock_acquire (&p->plock);
       swap_out(p);
       lock_release (&p->plock);
       lock_release (&glb_swap_lock);
       list_remove (e);
       free(f);
       i++;
    }
    e = list_next(e);
    if (e==list_end(&frames) && flipped)
      e = list_begin(&frames);
  }
  return true;
}

void *
frame_get_page(enum palloc_flags flags)
{
  lock_acquire (&frame_lock);
  uint8_t * kpage = palloc_get_page(flags);
  if (kpage == NULL)
  {
    //frame_evict();
    frame_evict_int(1);
    kpage = palloc_get_page(flags);
    if (kpage == NULL)
    {
      printf("palloc failed after frame_evict... this shoudn't happen\n");
      return NULL;
    }
  }
  lock_release (&frame_lock);
  return kpage;
}

bool
frame_thread_destroy (struct thread * t)
{
  if (list_size(&frames) <= 0)
  {
    return true;
  }

  struct frame * f;
  struct list_elem * e = list_begin(&frames);
  int i;

  for (i=0; e != list_end(&frames); e = list_next(e))
  {
    f = list_entry (e, struct frame, frame_elem);

    if (f->owner == t)
    {
      //palloc_free_page (f->paddr);
      //list_remove (e);
      //free(f);
    }
  }
  return true;
}

void
frame_dump (struct frame * f)
{
  frame_printf("& %x, vaddr %x paddr %x pte %x", f, f->vaddr, f->paddr, f->pte);
}

/* traverse a global frame list "shared_frames" to find the frame mapping to virtual address
  fault_addr. Returns pointer to the frame structure for the page, or NULL if not found */
struct frame *
frame_get_shared (void * fault_addr)
{
  struct list_elem * e = list_begin(&shared_frames);
  struct frame * ret_f = NULL;
  int i;
  lock_acquire (&frame_lock);
  for (e; e != list_end(&shared_frames); e = list_next (e))
  {
    struct frame * f = list_entry (e, struct frame, shared_frame_elem);
    if (f->vaddr == pg_round_down(fault_addr))
    {
     f->shared_cnt++;
     ret_f = f;
     break;
    }
  }
  lock_release (&frame_lock);

  return ret_f;
}
