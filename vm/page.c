#include <hash.h>
#include "threads/pte.h"
#include "threads/palloc.h"
#include "threads/synch.h"
#include "devices/block.h"
#include "vm/page.h"
#include "filesys/cache.h"

/* from PintOS website */
static unsigned page_hash (const struct hash_elem*, void * UNUSED);
static bool page_less (const struct hash_elem *,
  const struct hash_elem *, void * UNUSED);

/* supplementary page table */
struct hash pages;

struct lock hash_lock;

/* initialize the supplementary page table system */
bool
page_init (void)
{
  page_printf("initializing page hash\n");
  lock_init (&hash_lock);
  lock_init (&glb_swap_lock);
  lock_init (&glb_fs_lock);
  return hash_init (&pages, page_hash, page_less, NULL);
}

/* allocate and initialize a new page structure */
struct page *
init_page (void)
{
  struct page * p = malloc (sizeof (struct page));
  lock_init (&p->plock);
  p->flags &= 0;
  return p;
}

/* from PintOS website */
struct page *
page_lookup (const void * address)
{
  lock_acquire (&hash_lock);
  struct page p;
  struct hash_elem *e;
  p.pte = (void *)address;
  e = hash_find (&pages, &p.hash_elem);
  struct page * ret_p = e != NULL ? hash_entry (e, struct page, hash_elem) : NULL;
  lock_release (&hash_lock);
  return ret_p;
}

/* save page into hash */
bool
page_save (struct page * p)
{
  lock_acquire (&hash_lock);
  //lock_acquire (&p->page_lock);
  hash_insert (&pages, &p->hash_elem);
  //lock_release (&p->page_lock);
  lock_release (&hash_lock);
}

/* similar to static function lookup_page(...) from pagedir.c */
uint32_t
page_get_pte (uint32_t *pd, void * vaddr, bool create)
{
  //lock_acquire (&page_lock);
  uint32_t *pt, *pde;
  uint32_t retval = NULL;

  pde = pd + pd_no(vaddr);
  if (*pde == 0)
  {
    if (create)
    {
      pt = palloc_get_page (PAL_ZERO);
      if (pt == NULL)
        goto done;
      *pde = pde_create (pt);
    }
    else
      goto done;
  }
  pt = pde_get_pt (*pde);
  retval = &pt[pt_no (vaddr)];
done:
  //lock_release (&page_lock);
  return retval;
}

/* returns a pointer to struct page of a page containing thread t,
  or returns NULL if no such structures exist */
static struct page *
page_find_by_thread (struct thread *t)
{
  struct hash_iterator i;

  hash_first (&i, &pages);
  while (hash_next(&i))
  {
      struct page * p = hash_entry (hash_cur (&i), struct page, hash_elem);
      if (p->owner == t)
        return p;
  }
  return NULL;
}

/* searches through swap table, finds all instances of struct pages that
  belong to thread and destroy them */
bool
page_thread_destroy (struct thread * t)
{
  lock_acquire (&hash_lock);
  /* search thread's logical address space and list all PTEs, then
  use the PTEs to destroy corresponding hashses in sup-page tabel */
  struct page * p;
  while ((p=page_find_by_thread (t)) != NULL)
  {
    //printf("delete pte %x, thread %x\n", p->pte, t);
    struct frame * f = p->fs;
    if (f != NULL)
    {
      //printf("frame info p %x, p->fs %x, f %x, owner %x\n", p, p->fs , f, p->owner);
      //frame_dump(f);
      //f->page = NULL;
      //free (f);
    }
    hash_delete (&pages, &p->hash_elem);
    free (p);
  }
  lock_release (&hash_lock);
}

bool
page_destroy (struct page * p)
{
  lock_acquire (&hash_lock);
  if (p->fs != NULL)
    if (--p->fs->shared_cnt <= 0)
      frame_remove (p->fs);
  hash_delete (&pages, &p->hash_elem);
  free(p);
  lock_release (&hash_lock);
}

bool
page_set_frame (struct page * p, struct frame * f)
{
  p->fs = f;
//  printf("p_set_f p %x, p->fs %x owner %x\n", p, p->fs, p->owner);
}

/* this function gets called by exception.c and tries to
  set-up page memory depending on informations in supplementary
  page table. The corresponding page table must exist */
bool
page_set_up (struct page * p, struct thread * t, uint32_t * pte,
    uint32_t page_flags, uint32_t pte_flags, void * fault_addr)
{
  lock_acquire (&p->plock);
  bool success = false;

  uint8_t * kpage;

  struct frame * f = NULL;

  if (p->flags & PAGE_in_fs && !p->writable)
  {
//    printf("page is read-only, vaddr %x!\n", pg_round_down(fault_addr));
    f = frame_get_shared (fault_addr);
  }

  if (f == NULL)
  {
    f = init_frame();
    kpage = frame_get_page (PAL_USER | PAL_ZERO);
  }
  else
    kpage = f->paddr;

  lock_acquire (&f->flock);

  if (kpage == NULL)
  {
    printf("kpage is null, this means frame_evict somehow failed\n");
    goto done;
  }

  p->flags = 0;
  p->flags |= page_flags;

  if (page_flags & PAGE_in_swap)
  {
    lock_acquire (&glb_swap_lock);
    swap_in (p, kpage);
    lock_release (&glb_swap_lock);
  }
  else if (page_flags & PAGE_in_fs)
  {
    memset(kpage, 0, PGSIZE);
    if (p->read_bytes != 0)
      inode_read_at (p->inode, kpage, p->read_bytes, p->offset);
  }
  else // must be stack
  {
    memset(kpage, 0, PGSIZE);
  }
  lock_release (&f->flock);

  f->paddr = kpage;
  f->vaddr = pg_round_down (fault_addr);
  f->page = p;
  list_push_back (&f->page_list, &p->frame_list_elem);
  f->owner = t;

  p->pte = pte;
  p->owner = t;
  if (page_flags & PAGE_is_code)
    *(p->pte) = *pte = pte_create_user (kpage, p->writable);
  else
    *(p->pte) = *pte = pte_create_user (kpage, true);

  *p->pte |= pte_flags;

  frame_insert (f);
  page_set_frame(p, f);
  success = true;
done:
  lock_release (&p->plock);
  return success;
}

/* from PintOS website */
static unsigned
page_hash (const struct hash_elem * p_, void * aux UNUSED)
{
  const struct page *p = hash_entry (p_, struct page, hash_elem);
  return hash_bytes (&p->pte, sizeof p->pte);
}

/* from PintOS website */
static bool
page_less (const struct hash_elem *a_, const struct hash_elem *b_,
  void *aux UNUSED)
{
  const struct page *a = hash_entry (a_, struct page, hash_elem);
  const struct page *b = hash_entry (b_, struct page, hash_elem);
  return a->pte < b->pte;
}
