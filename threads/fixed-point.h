#ifndef __FIXED_POINT_H__
#define __FIXED_POINT_H__

#define p_factor 17
#define q_factor 14

#define f_factor (1<<q_factor)

#define int_to_fp(n) (n*f_factor)

#define fp_to_int_zero(x) (x/f_factor)

#define fp_to_int_near(x) ((x>=0)? (x+f_factor/2)/f_factor : (x-f_factor/2)/f_factor)

#define fp_add(x, y) (x+y)

#define fp_sub(x, y) (x-y)

#define fpi_add(x, n) (x+n*f_factor)

#define fpi_sub(x, n) (x-n*f_factor)

#define fp_mul(x, y) (((int64_t)x)*y/f_factor)

#define fpi_mul(x, n) (x*n)

#define fp_div(x, y) (((int64_t)x)*f_factor/y)

#define fpi_div(x, n) (x/n)

#endif
