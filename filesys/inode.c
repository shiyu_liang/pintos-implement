#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "filesys/cache.h"
#include "threads/malloc.h"
#include "threads/synch.h"

/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44
#define INODE_DISK_DATA_SIZE 119

struct lock create_lock;

/* On-disk inode.
   Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk
  {
    block_sector_t start;               /* First data sector. */
    off_t length;                       /* File size in bytes. */
    off_t read_length;
    unsigned magic;                     /* Magic number. */
    inode_type type;
    data_type data_type;
    uint32_t parent;                    /* sector of parent inode */
    uint32_t data[INODE_DISK_DATA_SIZE];
    uint32_t indirect_sector;
    uint32_t dindirect_sector;
  };

/* Returns the number of sectors to allocate for an inode SIZE
   bytes long. */
static inline size_t
bytes_to_sectors (off_t size)
{
  return DIV_ROUND_UP (size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode 
  {
    struct list_elem elem;              /* Element in inode list. */
    block_sector_t sector;              /* Sector number of disk location. */
    int open_cnt;                       /* Number of openers. */
    bool removed;                       /* True if deleted, false otherwise. */
    int deny_write_cnt;                 /* 0: writes ok, >0: deny writes. */
    data_type data_type;
    struct lock extend_lock;
    struct lock dir_lock;
    uint32_t parent;
  };

/* Returns the block device sector that contains byte offset POS
   within INODE.
   Returns -1 if INODE does not contain data for a byte at offset
   POS. */
static block_sector_t
byte_to_sector (const struct inode *inode, off_t pos, bool write) 
{
  ASSERT (inode != NULL);

  block_sector_t ret = -1;
  struct descripter * desc = cache_get_block (inode->sector, false);
  struct inode_disk * disk = desc->buffer;
  off_t length = (write)? disk->length: disk->read_length;
  if (pos < length)
  {
    size_t sector_idx = pos / BLOCK_SECTOR_SIZE;
    if (sector_idx < INODE_DISK_DATA_SIZE)
      ret = disk->data[sector_idx];
    else if (sector_idx < 2 * INODE_DISK_DATA_SIZE)
    {
      size_t s = disk->indirect_sector;
      release_shared (desc);
      desc = cache_get_block (s, false);
      disk = desc->buffer;
      ret = disk->data[sector_idx - INODE_DISK_DATA_SIZE];
    }
    else
    {
      size_t d_idx = (sector_idx - 2*INODE_DISK_DATA_SIZE)/INODE_DISK_DATA_SIZE;
      size_t i_idx = (sector_idx - (2 + d_idx)*INODE_DISK_DATA_SIZE);

      size_t s = disk->dindirect_sector;
      release_shared (desc);
      desc = cache_get_block (s, false);
      disk = desc->buffer;

      s = disk->data[d_idx];
      release_shared (desc);
      desc = cache_get_block (s, false);
      disk = desc->buffer;
      ret = disk->data[i_idx];
    }
  }
  release_shared (desc);
  return ret;
}

block_sector_t
byte_to_sector_pub (const struct inode *inode, off_t pos) 
{
  return byte_to_sector (inode, pos, false);
}

/* List of open inodes, so that opening a single inode twice
   returns the same `struct inode'. */
static struct list open_inodes;

static bool create_inode_recurse (struct inode_disk *, block_sector_t, inode_type, size_t);
static void remove_inode_recurse (struct inode_disk *);
static unsigned get_total_sectors (size_t);
static bool grow (struct inode_disk *, size_t);

/* Initializes the inode module. */
void
inode_init (void) 
{
  list_init (&open_inodes);
  lock_init (&create_lock);
}

/* Initializes an inode with LENGTH bytes of data and
   writes the new inode to sector SECTOR on the file system
   device.
   Returns true if successful.
   Returns false if memory or disk allocation fails. */
bool
inode_create (block_sector_t sector, off_t length, data_type type)
{
  lock_acquire (&create_lock);
  struct inode_disk *disk_inode = NULL;
  bool success = false;

  ASSERT (length >= 0);

  /* If this assertion fails, the inode structure is not exactly
     one sector in size, and you should fix that. */
  ASSERT (sizeof *disk_inode == BLOCK_SECTOR_SIZE);

  disk_inode = calloc (1, sizeof *disk_inode);
  if (disk_inode != NULL)
    {
      size_t sectors = bytes_to_sectors (length);
      disk_inode->length = length;
      disk_inode->read_length = length;
      disk_inode->magic = INODE_MAGIC;
      disk_inode->type = BASE_INODE;
      disk_inode->data_type = type;
      if (get_total_sectors(sectors) > free_map_available())
      {
        free (disk_inode);
        goto done;
      }
      success = create_inode_recurse (disk_inode, sector, BASE_INODE, sectors);
      free (disk_inode);
    }
done:
  lock_release (&create_lock);
  return success;
}

/* Reads an inode from SECTOR
   and returns a `struct inode' that contains it.
   Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector)
{
  struct list_elem *e;
  struct inode *inode;

  /* Check whether this inode is already open. */
  for (e = list_begin (&open_inodes); e != list_end (&open_inodes);
       e = list_next (e)) 
    {
      inode = list_entry (e, struct inode, elem);
      if (inode->sector == sector) 
        {
          inode_reopen (inode);
          return inode; 
        }
    }

  /* Allocate memory. */
  inode = malloc (sizeof *inode);
  if (inode == NULL)
    return NULL;

  /* Initialize. */
  list_push_front (&open_inodes, &inode->elem);
  inode->sector = sector;
  inode->open_cnt = 1;
  inode->deny_write_cnt = 0;
  inode->removed = false;
  //printf("inode on sector %d OPENED %d\n", inode->sector, inode->open_cnt);
  lock_init (&inode->extend_lock);
  lock_init (&inode->dir_lock);

  struct descripter * desc = cache_get_block (sector, false);
  struct inode_disk * disk = desc->buffer;

  //ASSERT (disk->magic == INODE_MAGIC);
  //ASSERT (disk->type == BASE_INODE);
  inode->data_type = disk->data_type;
  release_shared (desc);

  return inode;
}

/* Reopens and returns INODE. */
struct inode *
inode_reopen (struct inode *inode)
{
  if (inode != NULL)
    inode->open_cnt++;
  //printf("inode on sector %d reopened %d->%d\n", inode->sector, inode->open_cnt - 1, inode->open_cnt);
  return inode;
};
/* Returns INODE's inode number. */
block_sector_t
inode_get_inumber (const struct inode *inode)
{
  return inode->sector;
}

/* Closes INODE and writes it to disk.
   If this was the last reference to INODE, frees its memory.
   If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode) 
{
  /* Ignore null pointer. */
  if (inode == NULL)
    return;

  //printf("inode on sector %d closed %d->%d\n", inode->sector, inode->open_cnt, inode->open_cnt - 1);
  /* Release resources if this was the last opener. */
  if (--inode->open_cnt == 0)
    {
      /* Remove from inode list and release lock. */
      list_remove (&inode->elem);
 
      /* Deallocate blocks if removed. */
      if (inode->removed) 
        {
          struct inode_disk * disk = calloc (1, BLOCK_SECTOR_SIZE);
          cache_memread (inode->sector, 0, disk, BLOCK_SECTOR_SIZE);
          remove_inode_recurse (disk);
          free_map_release (inode->sector, 1);
        }

      free (inode); 
    }
}

/* Marks INODE to be deleted when it is closed by the last caller who
   has it open. */
void
inode_remove (struct inode *inode) 
{
  ASSERT (inode != NULL);
  inode->removed = true;
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
   Returns the number of bytes actually read, which may be less
   than SIZE if an error occurs or end of file is reached. */
off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset) 
{
  uint8_t *buffer = buffer_;
  off_t bytes_read = 0;
  uint8_t *bcache = NULL;

  while (size > 0) 
    {
      /* Disk sector to read, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset, false);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode, false) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      if (sector_idx == -1)
        break;
      /* Number of bytes to actually copy out of this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      cache_memread (sector_idx, sector_ofs, buffer + bytes_read, chunk_size);
      
      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_read += chunk_size;

      sector_idx = byte_to_sector (inode, offset, false);
      if (size && sector_idx != -1)
        cache_request_read_ahead (sector_idx);
    }

  return bytes_read;
}

/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
   Returns the number of bytes actually written, which may be
   less than SIZE if end of file is reached or an error occurs.
   (Normally a write at end of file would extend the inode, but
   growth is not yet implemented.) */
off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
                off_t offset) 
{
  const uint8_t *buffer = buffer_;
  off_t bytes_written = 0;
  uint8_t *bcache = NULL;
  bool do_grow = false;
  struct descripter * desc;
  struct inode_disk * disk;

  if (inode->deny_write_cnt)
    return 0;

  while (size > 0) 
    {
      /* Sector to write, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset, true);

      if (sector_idx == -1)
      {
        size_t sectors_present = bytes_to_sectors (inode_length (inode, true));
        size_t sectors_requested = bytes_to_sectors (size + offset);
        if (sectors_requested - sectors_present > free_map_available())
          break;

        do_grow = true;
        desc = cache_get_block (inode->sector, true);
        disk = desc->buffer;

        ASSERT (disk->magic == INODE_MAGIC);

        while (disk->length < offset + size)
        {
          if (!grow (disk, offset + size))
            break;
        }
        release_exclusive (desc);

        sector_idx = byte_to_sector (inode, offset, true);

        if (sector_idx == -1)
          break;
      }

      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode, true) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually write into this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      cache_memwrite (sector_idx, sector_ofs, buffer + bytes_written, chunk_size);

      if (do_grow)
      {
        desc = cache_get_block (inode->sector, true);
        disk = desc->buffer;
        disk->read_length = disk->length;
        release_exclusive (desc);
        do_grow = false;
      }

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_written += chunk_size;

      sector_idx = byte_to_sector (inode, offset, true);
      if (size && sector_idx != -1)
        cache_request_read_ahead (sector_idx);
    }

  return bytes_written;
}

/* Disables writes to INODE.
   May be called at most once per inode opener. */
void
inode_deny_write (struct inode *inode) 
{
  inode->deny_write_cnt++;
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
}

/* Re-enables writes to INODE.
   Must be called once by each inode opener who has called
   inode_deny_write() on the inode, before closing the inode. */
void
inode_allow_write (struct inode *inode) 
{
  ASSERT (inode->deny_write_cnt > 0);
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
  inode->deny_write_cnt--;
}

/* Returns the length, in bytes, of INODE's data. */
off_t
inode_length (const struct inode *inode, bool write)
{
  struct descripter * desc = cache_get_block (inode->sector, false);
  struct inode_disk * disk = desc->buffer;
  off_t ret = (write)? disk->length: disk->read_length;
  release_shared (desc);
  return ret;
}

/* write DISK to SECTOR and iteratively create SECTORS of data */
static bool
create_inode_recurse (struct inode_disk * disk, block_sector_t sector, inode_type type, size_t sectors)
{
  bool success = true;
  int i;
  static char zeros [BLOCK_SECTOR_SIZE];
  struct inode_disk * indir;
  struct inode_disk * dindir;


  disk->type = type;
  disk->magic = INODE_MAGIC;
  switch (type)
  {
    case BASE_INODE:
      if (sectors)
      {
        for (i = 0; i < sectors && i < INODE_DISK_DATA_SIZE; i++)
        {
          success &= free_map_allocate (1, &disk->data[i]);
          cache_memwrite (disk->data[i], 0, zeros, BLOCK_SECTOR_SIZE);
        }
      }
      if (sectors > INODE_DISK_DATA_SIZE)
      {
        indir = calloc (1, BLOCK_SECTOR_SIZE);
        if (indir == NULL)
        {
          printf("calloc failed\n");
          success = false;
          goto done;
        }
        indir->magic = INODE_MAGIC;
        success &= free_map_allocate (1, &disk->indirect_sector);
        success &= create_inode_recurse (indir, disk->indirect_sector, INDIRECT_INODE,
              (sectors > 2*INODE_DISK_DATA_SIZE)? INODE_DISK_DATA_SIZE: sectors - INODE_DISK_DATA_SIZE);
        free (indir);
      }
      if (sectors >= (2*INODE_DISK_DATA_SIZE))
      {
        dindir = calloc (1, BLOCK_SECTOR_SIZE);
        if (dindir == NULL)
        {
          success = false;
          goto done;
        }
        dindir->magic = INODE_MAGIC;
        success &= free_map_allocate (1, &disk->dindirect_sector);
        success &= create_inode_recurse (dindir, disk->dindirect_sector, DOUBLE_INDIRECT, sectors - 2*INODE_DISK_DATA_SIZE);
        free (dindir);
      } 
      goto done;
      break;
    case INDIRECT_INODE:
      if (sectors)
      {
        int i;
        for (i = 0; i < sectors && i < INODE_DISK_DATA_SIZE; i++)
        {
          success &= free_map_allocate (1, &disk->data[i]);
          cache_memwrite (disk->data[i], 0, zeros, BLOCK_SECTOR_SIZE);
        }
      }
      break;
    case DOUBLE_INDIRECT:
      i = 0;
      while (sectors > 0)
      {
        off_t secs = (sectors < INODE_DISK_DATA_SIZE)? sectors: INODE_DISK_DATA_SIZE;
        indir = calloc (1, BLOCK_SECTOR_SIZE);

        free_map_allocate (1, &disk->data[i]);
        create_inode_recurse (indir, disk->data[i], INDIRECT_INODE, secs);

        free (indir);
        i++;
        sectors -= secs;
      }
      break;
    default:
      break;
  };
done:
  cache_memwrite (sector, 0, disk,  BLOCK_SECTOR_SIZE);
  cache_flush ();
  return success;
}

static void
remove_inode_recurse (struct inode_disk * disk)
{
  int i;
  switch (disk->type)
  {
    case BASE_INODE:
      if (disk->dindirect_sector)
      {
        struct inode_disk * dindir = calloc (1, BLOCK_SECTOR_SIZE);
        cache_memread (disk->dindirect_sector, 0, dindir, BLOCK_SECTOR_SIZE);
        remove_inode_recurse (dindir);
        free (dindir);
        free_map_release (disk->dindirect_sector, 1);
      }
      if (disk->indirect_sector)
      {
        struct inode_disk * indir = calloc (1, BLOCK_SECTOR_SIZE);
        cache_memread (disk->indirect_sector, 0, indir, BLOCK_SECTOR_SIZE);
        remove_inode_recurse (indir);
        free (indir);
        free_map_release (disk->indirect_sector, 1);
      }
    case INDIRECT_INODE:
      for (i = 0; i < INODE_DISK_DATA_SIZE && disk->data[i]; i++)
        free_map_release (disk->data[i], 1);
      break;
    case DOUBLE_INDIRECT:
      for (i = 0; i < INODE_DISK_DATA_SIZE && disk->data[i]; i++)
      {
        struct inode_disk * indir = calloc (1, BLOCK_SECTOR_SIZE);
        cache_memread (disk->data[i], 0, indir, BLOCK_SECTOR_SIZE);
        remove_inode_recurse (indir);
        free (indir);
        free_map_release (disk->data[i], 1);
      }
      break;
    default:
      break;
  }
}

/* returns total number of sectors including metadata */
static unsigned
get_total_sectors (size_t sectors)
{
  if (sectors <= INODE_DISK_DATA_SIZE)
    return sectors + 1;
  if (sectors <= 2*INODE_DISK_DATA_SIZE)
    return sectors + 2;

  size_t d_i_cnt = DIV_ROUND_UP((sectors - 2*INODE_DISK_DATA_SIZE), INODE_DISK_DATA_SIZE);
  return sectors + 3 + d_i_cnt;
}

/* grow DISK to contain enough sectors to contain TARGET bytes of 
  data, one sector at a time */
static bool
grow (struct inode_disk * disk, size_t target_length)
{
  ASSERT (disk->magic == INODE_MAGIC);
  ASSERT (disk->type == BASE_INODE);
  bool success = false;
  off_t current_sectors = bytes_to_sectors (disk->length);
  off_t sector_ofs = disk->length % BLOCK_SECTOR_SIZE; // offset within sector
  off_t chunk;

  static char zeros[BLOCK_SECTOR_SIZE];

  if (target_length > disk->length)
  {
    off_t sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
    off_t inode_left = target_length - disk->length;
    chunk = (sector_left > inode_left)? inode_left: sector_left;

    off_t new_sectors = bytes_to_sectors (disk->length + chunk);
    if (new_sectors != current_sectors)
    {
      if (current_sectors < INODE_DISK_DATA_SIZE)
      {
        if (!free_map_allocate (1, &disk->data[current_sectors]))
          goto done;
        cache_memwrite (disk->data[current_sectors], 0, zeros, BLOCK_SECTOR_SIZE);
        success = true;
        goto done;
      }
      else if (current_sectors < 2*INODE_DISK_DATA_SIZE)
      {
        off_t ind_idx = current_sectors - INODE_DISK_DATA_SIZE;

        struct inode_disk * indir = calloc (1, BLOCK_SECTOR_SIZE);
        if (disk->indirect_sector == 0)
        {
          if (!free_map_allocate (1, &disk->indirect_sector))
          {
            free (indir);
            goto done;
          }
          indir->magic = INODE_MAGIC;
          indir->type = INDIRECT_INODE;
        }
        else
          cache_memread (disk->indirect_sector, 0, indir, BLOCK_SECTOR_SIZE);

        ASSERT (indir->magic = INODE_MAGIC);
        ASSERT (indir->type = INDIRECT_INODE);

        if (!free_map_allocate (1, &indir->data[ind_idx]))
        {
          free (indir);
          goto done;
        }

        cache_memwrite (disk->indirect_sector, 0, indir, BLOCK_SECTOR_SIZE);
        cache_memwrite (indir->data[ind_idx], 0, zeros, BLOCK_SECTOR_SIZE);

        free (indir);
        success = true;
        goto done;
      }
      else
      {
        size_t dind_sector_offs = current_sectors - 2 * INODE_DISK_DATA_SIZE;
        size_t ind_sector_offs = dind_sector_offs / INODE_DISK_DATA_SIZE;
        size_t data_sector_offs = dind_sector_offs % INODE_DISK_DATA_SIZE;

        success = true;

        struct inode_disk * dindir = calloc (1, BLOCK_SECTOR_SIZE);
        struct inode_disk * indir = calloc (1, BLOCK_SECTOR_SIZE);

        if (dind_sector_offs == 0)
        {
          ASSERT (disk->dindirect_sector == 0);
          success &= free_map_allocate (1, &disk->dindirect_sector);
          dindir->magic = INODE_MAGIC;
          dindir->type = DOUBLE_INDIRECT;
        }
        else
          cache_memread (disk->dindirect_sector, 0, dindir, BLOCK_SECTOR_SIZE);

        if (data_sector_offs == 0)
        {
          success &= free_map_allocate (1, &dindir->data[ind_sector_offs]);
          indir->magic = INODE_MAGIC;
          indir->type = INDIRECT_INODE;
        }
        else
          cache_memread (dindir->data[ind_sector_offs], 0, indir, BLOCK_SECTOR_SIZE);

        success &= free_map_allocate (1, &indir->data[data_sector_offs]);

        cache_memwrite (disk->dindirect_sector, 0, dindir, BLOCK_SECTOR_SIZE);
        cache_memwrite (dindir->data[ind_sector_offs], 0, indir, BLOCK_SECTOR_SIZE);
        cache_memwrite (indir->data[data_sector_offs], 0, zeros, BLOCK_SECTOR_SIZE);

        free (dindir);
        free (indir);

        goto done;
      }
    }
    else if (disk->length != target_length)
      success = true;
  }

done:
  disk->length += chunk;
  return success;
}

data_type
inode_get_type (const struct inode * inode)
{
  return inode->data_type;
}

void
inode_dir_lock_acquire (struct inode * inode)
{
  lock_acquire (&inode->dir_lock);
}

void
inode_dir_lock_release (struct inode * inode)
{
  lock_release (&inode->dir_lock);
}

int inode_open_cnt (struct inode * inode)
{
  return inode->open_cnt;
}

bool
inode_set_parent (block_sector_t target, block_sector_t parent)
{
  struct descripter * d = cache_get_block (target, true);
  struct inode_disk * disk = d->buffer;
  disk->parent = parent;
  //printf("set parent of %d to %d\n", target, parent);
  release_exclusive (d);
}

block_sector_t
inode_get_parent (block_sector_t sector)
{
  block_sector_t parent_sector;
  if (sector == ROOT_DIR_SECTOR)
    return sector;
  struct descripter * d = cache_get_block (sector, false);
  struct inode_disk * disk = d->buffer;
  parent_sector = disk->parent;
  release_shared (d);
  return parent_sector;
}
