#include "filesys/directory.h"
#include <stdio.h>
#include <string.h>
#include <list.h>
#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "threads/malloc.h"
#include "threads/thread.h"

/* A single directory entry. */
struct dir_entry 
  {
    block_sector_t inode_sector;        /* Sector number of header. */
    char name[NAME_MAX + 1];            /* Null terminated file name. */
    bool in_use;                        /* In use or free? */
  };

static bool traverse (struct vfs *, char *, char target[NAME_MAX + 1]); /* resolve path string */
static void dir_lock_acquire (struct vfs *);  /* acquire lock on directory */
static void dir_lock_release (struct vfs *);  /* release lock on directory */

/* Creates a directory with space for ENTRY_CNT entries in the
   given SECTOR.  Returns true if successful, false on failure. */
bool
dir_create (block_sector_t sector, size_t entry_cnt)
{
  return inode_create (sector, entry_cnt * sizeof (struct dir_entry), DIRECTORY);
}

bool
dir_mkdir (const char *name) 
{
  block_sector_t inode_sector = 0;
  struct dir *dir = dir_get_pwd ();

  int stage = 0;
  if (dir == NULL)
    goto fail;

  stage = 1;
  if (!free_map_allocate (1, &inode_sector))
    goto fail;

  stage = 2;
  if (inode_sector == 0)
    goto fail;

  stage = 3;
  if (!inode_create (inode_sector, 0, DIRECTORY))
    goto fail;

  stage = 4;
  if (!dir_add (dir, name, inode_sector))
    goto fail;

  goto pass;

fail:
  if (inode_sector != 0) 
    free_map_release (inode_sector, 1);
  return false;

pass:
  return true;
}

/* Opens and returns the directory for the given INODE, of which
   it takes ownership.  Returns a null pointer on failure. */
struct vfs *
dir_open (struct inode *inode) 
{
  struct vfs *dir = calloc (1, sizeof *dir);
  if (inode != NULL && dir != NULL)
    {
      dir->inode = inode;
      dir->pos = 0;
      dir->type = DIRECTORY;
      return dir;
    }
  else
    {
      inode_close (inode);
      free (dir);
      return NULL; 
    }
}

/* Opens the root directory and returns a directory for it.
   Return true if successful, false on failure. */
struct vfs *
dir_open_root (void)
{
  return dir_open (inode_open (ROOT_DIR_SECTOR));
}

struct vfs *
dir_get_pwd (void)
{
  return (struct vfs *) thread_current()->pwd;
}

/* Opens and returns a new directory for the same inode as DIR.
   Returns a null pointer on failure. */
struct vfs *
dir_reopen (struct vfs *dir) 
{
  return dir_open (inode_reopen (dir->inode));
}

/* Destroys DIR and frees associated resources. */
void
dir_close (struct vfs *dir) 
{
  if (dir != NULL)
    {
      inode_close (dir->inode);
      free (dir);
    }
}

/* Returns the inode encapsulated by DIR. */
struct inode *
dir_get_inode (struct vfs *dir) 
{
  return dir->inode;
}

/* Searches DIR for a file with the given NAME.
   If successful, returns true, sets *EP to the directory entry
   if EP is non-null, and sets *OFSP to the byte offset of the
   directory entry if OFSP is non-null.
   otherwise, returns false and ignores EP and OFSP. */
static bool
lookup (const struct vfs *dir, const char *name,
        struct dir_entry *ep, off_t *ofsp) 
{
  struct dir_entry e;
  size_t ofs;
  struct vfs * work_dir;
  
  ASSERT (dir != NULL);
  ASSERT (name != NULL);
  ASSERT (strlen (name));

  for (ofs = 0; inode_read_at (dir->inode, &e, sizeof e, ofs) == sizeof e;
       ofs += sizeof e) 
    if (e.in_use && !strcmp (name, e.name)) 
      {
        if (ep != NULL)
          *ep = e;
        if (ofsp != NULL)
          *ofsp = ofs;
        return true;
      }
  return false;
}

/* find item NAME in directory D
  set D to directory containing the item and
  set ITEM to name of item without path variables,
  any opened directory structures during this function are closed */
static bool
traverse (struct vfs * d, char * name, char target[NAME_MAX + 1])
{
  off_t total_length = strlen (name);
  off_t tmp_length = 0;
  char *item, *save_ptr;

  if (name[0] == '/' && total_length == 1)
  {
    dir_close (d);
    d = dir_open_root ();
    strlcpy (target, name, NAME_MAX);
    goto done;
  }

  if (name[0] == '/') // absolute path
  {
    dir_close (d);
    d = dir_open_root ();
  }

  int slash_cnt = 0;
  int i;
  for (i = 0; i < strlen (name); i++)
    if (name[i] == '/')
      slash_cnt++;

  //printf("total length %d, slash_cnt %d\n", total_length, slash_cnt);

  for (item = strtok_r (name, "/", &save_ptr); item != NULL;
       item = strtok_r (NULL, "/", &save_ptr))
  {
    tmp_length += strlen (item);
    //printf("item is %s, tmp_length %d\n", item, tmp_length);

    if (tmp_length >= total_length - slash_cnt)
      break;
    else
    {
      if (!strcmp (item, "."))
      {
        continue; // do not update d
      }
      if (!strcmp (item, ".."))
      {
        //printf("parent\n");
        block_sector_t b = inode_get_parent (
          inode_get_inumber (d->inode));
        dir_close (d);
        d = dir_open (inode_open (b));
        continue;
      }

      // update dir
      struct inode * inode;
      struct dir_entry e;

      if (lookup (d, item, &e, NULL))
        inode = inode_open (e.inode_sector);
      else
        inode = NULL;

      if (inode != NULL && inode_get_type (inode) == DIRECTORY) 
      {
        dir_close (d);
        d = dir_open (inode);
      }
    }
  }

  strlcpy (target, item, NAME_MAX);
done:
  return true;
}

/* Searches DIR for a file with the given NAME
   and returns true if one exists, false otherwise.
   On success, sets *INODE to an inode for the file, otherwise to
   a null pointer.  The caller must close *INODE. */
bool
dir_lookup (const struct vfs *dir, const char *name,
            struct inode **inode) 
{
  struct dir_entry e;

  ASSERT (dir != NULL);
  ASSERT (name != NULL);

  char * fn = calloc (1, strlen(name) + 1);
  strlcpy (fn, name, strlen(name) + 1);

  struct vfs * tmp_dir = dir_reopen (dir);

  char dir_name [NAME_MAX + 1];

  if (!traverse (tmp_dir, fn, dir_name))
    goto done;

  /* special case for root directory '/',
    add a check here to open an inode for the root
    directory. We dont' need such check in dir_add and
    dir_remove because add and remove operations targetting
    the root must fail */
  if (!strcmp (dir_name, "/"))
  {
    *inode = inode_open (ROOT_DIR_SECTOR);
    goto done;
  }

  if (!strcmp (dir_name, "."))
  {
    *inode = inode_reopen (tmp_dir->inode);
    goto done;
  }

  if (!strcmp (dir_name, ".."))
  {
    block_sector_t parent_sector;
    *inode = dir_get_inode (tmp_dir);
    parent_sector = inode_get_inumber (*inode);
    parent_sector = inode_get_parent (parent_sector);
    *inode = inode_open (parent_sector);
    goto done;
  }

  dir_lock_acquire (tmp_dir);
  if (lookup (tmp_dir, dir_name, &e, NULL))
    *inode = inode_open (e.inode_sector);
  else
    *inode = NULL;
  dir_lock_release (tmp_dir);
done:
  free (fn);
  dir_close (tmp_dir);

  return *inode != NULL;
}

/* Adds a file named NAME to DIR, which must not already contain a
   file by that name.  The file's inode is in sector
   INODE_SECTOR.
   Returns true if successful, false on failure.
   Fails if NAME is invalid (i.e. too long) or a disk or memory
   error occurs. */
bool
dir_add (struct vfs *dir, const char *name, block_sector_t inode_sector)
{
  struct dir_entry e;
  off_t ofs;
  bool success = false;

  ASSERT (dir != NULL);
  ASSERT (name != NULL);

  char * fn = calloc (1, strlen(name) + 1);
  strlcpy (fn, name, strlen(name) + 1);

  struct vfs * tmp_dir = dir_reopen (dir);

  char dir_name [NAME_MAX + 1];

  if (!traverse (tmp_dir, fn, dir_name))
    goto done;

  dir_lock_acquire (tmp_dir);
  /* Check that NAME is not in use. */
  if (lookup (tmp_dir, dir_name, NULL, NULL))
    goto done_l;

  /* Set OFS to offset of free slot.
     If there are no free slots, then it will be set to the
     current end-of-file.
     
     inode_read_at() will only return a short read at end of file.
     Otherwise, we'd need to verify that we didn't get a short
     read due to something intermittent such as low memory. */
  for (ofs = 0; inode_read_at (tmp_dir->inode, &e, sizeof e, ofs) == sizeof e;
       ofs += sizeof e) 
    if (!e.in_use)
      break;

  /* Write slot. */
  e.in_use = true;
  strlcpy (e.name, dir_name, sizeof e.name);
  e.inode_sector = inode_sector;
  success = inode_write_at (tmp_dir->inode, &e, sizeof e, ofs) == sizeof e;
  if (success)
    inode_set_parent (inode_sector, inode_get_inumber(tmp_dir->inode));

done_l:
  dir_lock_release (tmp_dir);
done:
  dir_close (tmp_dir);
  free (fn);
  return success;
}

/* Removes any entry for NAME in DIR.
   Returns true if successful, false on failure,
   which occurs only if there is no file with the given NAME. */
bool
dir_remove (struct vfs *dir, const char *name) 
{
  struct dir_entry e;
  struct inode *inode = NULL;
  bool success = false;
  off_t ofs;

  ASSERT (dir != NULL);
  ASSERT (name != NULL);

  char * fn = calloc (1, strlen(name) + 1);
  strlcpy (fn, name, strlen(name) + 1);

  struct vfs * tmp_dir = dir_reopen (dir);

  char dir_name [NAME_MAX + 1];

  if (!traverse (tmp_dir, fn, dir_name))
    goto done;

  /* Find directory entry. */
  if (!lookup (tmp_dir, dir_name, &e, &ofs))
    goto done;

  dir_lock_acquire (tmp_dir);
  /* Open inode. */
  inode = inode_open (e.inode_sector);
  //printf("!!! trying to remove %s on sector %d, open_cnt %d\n",
  //  dir_name, e.inode_sector, inode_open_cnt (inode));
  if (inode_open_cnt (inode) > 1 && inode_get_type (inode) == DIRECTORY)
    goto done_l;
  if (inode == NULL)
    goto done_l;

  /* check if directory is empty */
  if (inode_get_type(inode) == DIRECTORY)
  {
    char entry_name [NAME_MAX + 1];
    struct vfs * rmdir = dir_open (inode);
    if (dir_readdir (rmdir, entry_name))
    {
      dir_close (rmdir);
      goto done_l;
    }
    dir_close (rmdir);
  }

  /* Erase directory entry. */
  e.in_use = false;
  if (inode_write_at (tmp_dir->inode, &e, sizeof e, ofs) != sizeof e) 
    goto done_l;

  /* Remove inode. */
  success = true;

done_l:
  dir_lock_release (tmp_dir);
  inode_remove (inode);
  inode_close (inode);
done:
  dir_close (tmp_dir);
  return success;
}

/* Reads the next directory entry in DIR and stores the name in
   NAME.  Returns true if successful, false if the directory
   contains no more entries. */
bool
dir_readdir (struct vfs *dir, char name[NAME_MAX + 1])
{
  struct dir_entry e;

  while (inode_read_at (dir->inode, &e, sizeof e, dir->pos) == sizeof e) 
    {
      dir->pos += sizeof e;
      if (e.in_use)
        {
          strlcpy (name, e.name, NAME_MAX + 1);
          return true;
        } 
    }
  return false;
}

static void dir_lock_acquire (struct vfs * dir)
{
  inode_dir_lock_acquire (dir->inode);
}

static void dir_lock_release (struct vfs * dir)
{
  inode_dir_lock_release (dir->inode);
}

void dir_ls (struct vfs * dir)
{
  char name [NAME_MAX];
  struct vfs * wkdir = dir_reopen (dir);
  while (dir_readdir (wkdir, name))
    printf("./%s\n", name);
  dir_close (wkdir);
}
