#ifndef FILESYS_INODE_H
#define FILESYS_INODE_H

#include <stdbool.h>
#include "filesys/off_t.h"
#include "devices/block.h"

struct bitmap;
typedef enum {FILE, DIRECTORY} data_type;
typedef enum {BASE_INODE, INDIRECT_INODE, DOUBLE_INDIRECT} inode_type;

void inode_init (void);
bool inode_create (block_sector_t, off_t, data_type);
struct inode *inode_open (block_sector_t);
struct inode *inode_reopen (struct inode *);
block_sector_t inode_get_inumber (const struct inode *);
void inode_close (struct inode *);
void inode_remove (struct inode *);
off_t inode_read_at (struct inode *, void *, off_t size, off_t offset);
off_t inode_write_at (struct inode *, const void *, off_t size, off_t offset);
void inode_deny_write (struct inode *);
void inode_allow_write (struct inode *);
off_t inode_length (const struct inode *, bool);
data_type inode_get_type (const struct inode *);

block_sector_t byte_to_sector_pub (const struct inode *inode, off_t pos);

void inode_dir_lock_acquire (struct inode *);
void inode_dir_lock_release (struct inode *);

int inode_open_cnt (struct inode *);

bool inode_set_parent (block_sector_t, block_sector_t);
block_sector_t inode_get_parent (block_sector_t);

#endif /* filesys/inode.h */
