#include "filesys/filesys.h"
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "threads/thread.h"
#include "filesys/file.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"
#include "filesys/directory.h"
#include "filesys/cache.h"

/* Partition that contains the file system. */
struct block *fs_device;

static void do_format (void);

/* Initializes the file system module.
   If FORMAT is true, reformats the file system. */
void
filesys_init (bool format) 
{
  fs_device = block_get_role (BLOCK_FILESYS);
  if (fs_device == NULL)
    PANIC ("No file system device found, can't initialize file system.");

  inode_init ();
  free_map_init ();

  if (format) 
    do_format ();

  free_map_open ();
  thread_current ()->pwd = dir_open_root ();
}

/* Shuts down the file system module, writing any unwritten data
   to disk. */
void
filesys_done (void) 
{
  printf("closing free map\n");
  free_map_close ();
  cache_flush ();
  cache_shutdown ();
}

/* Creates a file named NAME with the given INITIAL_SIZE.
   Returns true if successful, false otherwise.
   Fails if a file named NAME already exists,
   or if internal memory allocation fails. */
bool
filesys_create (const char *name, off_t initial_size) 
{
  block_sector_t inode_sector = 0;
  int stage = 0;
  struct dir *dir = dir_get_pwd ();
  if (dir == NULL)
    goto fail;

  stage = 1;
  if (!free_map_allocate (1, &inode_sector))
    goto fail;

  stage = 2;
  if (!inode_create (inode_sector, initial_size, FILE))
    goto fail;

  stage = 3;
  if (!dir_add (dir, name, inode_sector))
    goto fail;

  stage = 4;
  goto pass;

fail:
  if (inode_sector != 0) 
  return false;

pass:
  return true;
}

/* Opens the file with the given NAME.
   Returns the new file if successful or a null pointer
   otherwise.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
struct file *
filesys_open (const char *name)
{
  struct dir *dir = dir_get_pwd ();
  struct inode *inode = NULL;

  if (dir != NULL)
    dir_lookup (dir, name, &inode);

  if (inode == NULL)
    return NULL;

  if (inode_get_type (inode) == DIRECTORY)
    return dir_open (inode);
  else
    return file_open (inode);
}

/* Deletes the file named NAME.
   Returns true if successful, false on failure.
   Fails if no file named NAME exists,
   or if an internal memory allocation fails. */
bool
filesys_remove (const char *name) 
{
  struct dir *dir = dir_get_pwd ();
  bool success = dir != NULL && dir_remove (dir, name);

  return success;
}

/* Formats the file system. */
static void
do_format (void)
{
  printf ("filesys: formatting file system\n");
  free_map_create ();
  if (!dir_create (ROOT_DIR_SECTOR, 16))
    PANIC ("root directory creation failed");
  free_map_close ();
  cache_flush ();
}
