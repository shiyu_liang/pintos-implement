#ifndef FILESYS_FILE_H
#define FILESYS_FILE_H

#include "filesys/off_t.h"
#include "filesys/filesys.h"

struct inode;

/* Opening and closing files. */
struct vfs *file_open (struct inode *);
struct vfs *file_reopen (struct vfs *);
void file_close (struct vfs *);
struct inode *file_get_inode (struct vfs *);

/* Reading and writing. */
off_t file_read (struct vfs *, void *, off_t);
off_t file_read_at (struct vfs *, void *, off_t size, off_t start);
off_t file_write (struct vfs *, const void *, off_t);
off_t file_write_at (struct vfs *, const void *, off_t size, off_t start);

/* Preventing writes. */
void file_deny_write (struct vfs *);
void file_allow_write (struct vfs *);

/* File position. */
void file_seek (struct vfs *, off_t);
off_t file_tell (struct vfs *);
off_t file_length (struct vfs *);

#endif /* filesys/file.h */
