#include <stdio.h>
#include <debug.h>
#include <hash.h>
#include <string.h>
#include "devices/block.h"
#include "devices/timer.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "threads/palloc.h"
#include "threads/malloc.h"
#include "threads/vaddr.h"
#include "filesys/cache.h"
#include "filesys/filesys.h"	// fs_device

struct cache_block
{
	uint32_t data[128];
};

struct request
{
	struct list_elem elem;
	block_sector_t sector;
};

struct list descripters;
struct list requests;

uint8_t * cache;
struct lock io_lock;
struct lock request_lock;
bool running;

static void cache_pflush (void *);
static void cache_read_ahead (void *);

static struct descripter *
init_descripter (void)
{
	struct descripter * d = calloc (1, sizeof * d);
	cond_init (&d->s_lock.cond);
	lock_init (&d->s_lock.lock);
	d->s_lock.cnt = 0;
	return d;
}

void
acquire_shared (struct descripter * d)
{
	lock_acquire (&d->s_lock.lock);
	d->s_lock.read_waiter_cnt++;
	while (d->s_lock.cnt < 0 || d->s_lock.deny_read)
		cond_wait (&d->s_lock.cond, &d->s_lock.lock);
	d->s_lock.read_waiter_cnt--;
	d->s_lock.cnt++;
	d->locked = true;
	lock_release (&d->s_lock.lock);
}

void
acquire_exclusive (struct descripter * d)
{
	lock_acquire (&d->s_lock.lock);
	d->s_lock.write_waiter_cnt++;
	while (d->s_lock.cnt)
		cond_wait (&d->s_lock.cond, &d->s_lock.lock);
	if (d->s_lock.read_waiter_cnt > 5)
		d->s_lock.deny_read = true;
	d->s_lock.write_waiter_cnt--;
	d->s_lock.cnt = -1;
	d->locked = true;
	lock_release (&d->s_lock.lock);
}

void
release_shared (struct descripter * d)
{
	lock_acquire (&d->s_lock.lock);
	d->s_lock.cnt--;
	if (!d->s_lock.cnt)
		d->locked = false;
	cond_signal (&d->s_lock.cond, &d->s_lock.lock);
	lock_release (&d->s_lock.lock);
}

void
release_exclusive (struct descripter * d)
{
	lock_acquire (&d->s_lock.lock);
	d->s_lock.cnt = 0;
	d->s_lock.deny_read = false;
	d->locked = false;
	cond_broadcast (&d->s_lock.cond, &d->s_lock.lock);
	lock_release (&d->s_lock.lock);
}

static void
evict (struct descripter * d)
{
	if (d->valid)
	{
		acquire_exclusive (d);
		if (d->dirty)
			block_write (fs_device, d->sector, d->buffer);
		d->valid = false;
		d->dirty = false;
		d->sector = -1;
		release_exclusive (d);
	}
}

void
cache_init (void)
{
	cache_printf("initializing buffer cache\n");
	list_init (&descripters);
	list_init (&requests);

	cache = palloc_get_multiple (PAL_ZERO, CACHE_SIZE*BLOCK_SECTOR_SIZE/PGSIZE);

	lock_init (&io_lock);
	lock_init (&request_lock);

	lock_acquire (&io_lock);
	int i;
	for (i = 0; i < CACHE_SIZE; i++)
	{
		struct descripter * d = init_descripter ();
		d->sector = -1;
		d->buffer = cache + i * BLOCK_SECTOR_SIZE;
		list_push_back (&descripters, &d->elem);
	}
	lock_release (&io_lock);

	running = true;
	thread_create ("flusher", PRI_DEFAULT, cache_pflush, NULL);
	thread_create ("read_ahead", PRI_DEFAULT, cache_read_ahead, NULL);
}

void
cache_shutdown (void)
{
	lock_acquire (&io_lock);
	running = false;
	struct list_elem * e;
	struct descripter * d;
	palloc_free_multiple (cache, CACHE_SIZE*BLOCK_SECTOR_SIZE/PGSIZE);
	while (list_size (&descripters))
	{
		e = list_pop_front (&descripters);
		d = list_entry (e, struct descripter, elem);
		free (d);
	}
	lock_release (&io_lock);
}

void
cache_flush (void)
{
	lock_acquire (&io_lock);
	struct list_elem * e;
	struct descripter * d;
	for (e = list_begin (&descripters); e != list_end (&descripters); e = list_next (e))
	{
		d = list_entry (e, struct descripter, elem);
		if (d->dirty)
		{
			block_write (fs_device, d->sector, d->buffer);
			d->dirty = false;
		}
	}
	lock_release (&io_lock);
}

struct descripter *
cache_get_block (block_sector_t sector, bool write)
{
	lock_acquire (&io_lock);

	struct descripter * desc;
	struct descripter * oldest_invalid = NULL;
	struct list_elem * e;

	for ( e = list_begin(&descripters); e != list_end (&descripters); e = list_next (e) )
	{
		desc = list_entry (e, struct descripter, elem);
		if (desc->sector == sector)
			goto found;
		if (!desc->valid && oldest_invalid == NULL)
			oldest_invalid = desc;
	}

	if (oldest_invalid)
		desc = oldest_invalid;
	else // full, must evict one
	{
		e = list_begin (&descripters);
		desc = list_entry (e, struct descripter, elem);
		while (desc->locked)
		{
			if ( e == list_end (&descripters) )
				PANIC ("all buffer caches are locked !!!\n");
			e = list_next (e);
			desc = list_entry (e, struct descripter, elem);
		}
		evict (desc);
	}

found:
	acquire_exclusive (desc);
	if (!desc->valid)
		block_read (fs_device, sector, desc->buffer);

	list_remove (&desc->elem);
	list_push_back (&descripters, &desc->elem);

	desc->dirty |= write;
	desc->valid = true;
	desc->sector = sector;
	release_exclusive (desc);
	lock_release (&io_lock);

	if (write)
		acquire_exclusive (desc);
	else
		acquire_shared (desc);

	return desc;
}

void
cache_memread (block_sector_t s, size_t offset, void * buffer, size_t size)
{
	struct descripter * d = cache_get_block (s, false);
	uint8_t * target = (uint8_t * )d->buffer + offset;
	memcpy (buffer, target, size);
	release_shared (d);
}

void
cache_memwrite (block_sector_t s, size_t offset, void * buffer, size_t size)
{
	struct descripter * d = cache_get_block (s, true);
	uint8_t * target = (uint8_t *)d->buffer + offset;
	memcpy (target, buffer, size);
	release_exclusive (d);
}

void
cache_request_read_ahead (block_sector_t sector)
{
	lock_acquire (&request_lock);
	struct request * r = calloc (1, sizeof * r);
	r->sector = sector;
	list_push_back (&requests, &r->elem);
	lock_release (&request_lock);
}

static void
cache_pflush (void * arg UNUSED)
{
	while (running)
	{
		cache_flush ();
		timer_sleep (1500);
	}
	thread_exit ();
}

static void
cache_read_ahead (void * arg UNUSED)
{
	struct request * r;
	struct list_elem * e;
	struct descripter * d;
	while (running)
	{
		lock_acquire (&request_lock);
		while (list_size (&requests))
		{
			e = list_pop_front (&requests);
			r = list_entry (e, struct request, elem);
			d = cache_get_block (r->sector, false);
			release_shared (d);
			free (r);
		}
		lock_release (&request_lock);
		timer_sleep (1500);
	}
	thread_exit ();
}
