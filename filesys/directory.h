#ifndef FILESYS_DIRECTORY_H
#define FILESYS_DIRECTORY_H

#include <stdbool.h>
#include <stddef.h>
#include "devices/block.h"
#include "threads/synch.h"
#include "filesys/filesys.h"

/* Maximum length of a file name component.
   This is the traditional UNIX maximum length.
   After directories are implemented, this maximum length may be
   retained, but much longer full path names must be allowed. */
#define NAME_MAX 20

struct inode;

/* Opening and closing directories. */
bool dir_create (block_sector_t sector, size_t entry_cnt);
bool dir_mkdir (const char *);
struct vfs *dir_open (struct inode *);
struct vfs *dir_open_root (void);
struct vfs *dir_get_pwd (void);
struct vfs *dir_reopen (struct vfs *);
void dir_close (struct vfs *);
struct inode *dir_get_inode (struct vfs *);

/* Reading and writing. */
bool dir_lookup (const struct vfs *, const char *name, struct inode **);
bool dir_add (struct vfs *, const char *name, block_sector_t);
bool dir_remove (struct vfs *, const char *name);
bool dir_readdir (struct vfs *, char name[NAME_MAX + 1]);

void dir_ls (struct vfs *);

#endif /* filesys/directory.h */
