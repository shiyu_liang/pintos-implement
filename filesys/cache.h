#ifndef FILESYS_CACHE_H
#define FILESYS_CACHE_H

#include <stdbool.h>
#include "devices/block.h"
#include "threads/synch.h"

#define CACHE_SIZE 64
#define cache_printf(...) printf("cache: " __VA_ARGS__)

struct shared_lock
{
	int cnt;
	struct lock lock;
	struct condition cond;
	bool deny_read;
	int read_waiter_cnt;
	int write_waiter_cnt;
};

struct descripter
{
	block_sector_t sector;
	struct cache_block * buffer;
	struct list_elem elem;
	bool dirty, valid;
	struct shared_lock s_lock;
	bool locked;
};

void acquire_shared (struct descripter * d);
void acquire_exclusive (struct descripter * d);
void release_shared (struct descripter * d);
void release_exclusive (struct descripter * d);

void cache_init (void);
void cache_shutdown (void);

struct descripter * cache_get_block (block_sector_t, bool);
void cache_flush (void);
void cache_memread (block_sector_t, size_t, void *, size_t);
void cache_memwrite (block_sector_t, size_t, void *, size_t);
void cache_request_read_ahead (block_sector_t);

#endif
