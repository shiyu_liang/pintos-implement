#ifndef __PROCESS_INFO__
#define __PROCESS_INFO__

#include <list.h>
#include "threads/synch.h"
#include "threads/thread.h"
#include "userprog/process.h"

typedef int pid_t;

struct process_info
{
   pid_t pid;
   char tname[30];

   struct semaphore child_load_sema;
   bool load_success;

   struct thread * parent;
   struct thread * thread;

   struct semaphore exit_sema; /* synchronization on wait() and exit() */
   struct semaphore exit_guard; /* make exit() an atomic operation, whether it was called by process or through exception */
   bool wait_called;
   int exit_status;
   bool exit_status_set;

   struct list children_list;
   struct list_elem celem;
};

#endif
