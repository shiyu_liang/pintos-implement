#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "filesys/directory.h"
#include "userprog/syscall.h"
#include "userprog/process.h"
#include "userprog/process_info.h"
#include "userprog/pagedir.h"
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/pte.h"
#include "vm/page.h"
#include "devices/shutdown.h"
#include "devices/input.h"

typedef int mapid_t;

static void syscall_handler (struct intr_frame *);

static void _halt(void);
static void _exit(int);
static pid_t _exec(const char*);
static int _wait(pid_t);
static bool _create(const char*, unsigned);
static bool _remove(const char*);
static int _open(const char*);
static int _filesize(int);
static int _read(int, void*, unsigned);
static int _write(int, const void*, unsigned);
static void _seek(int, unsigned);
static unsigned _tell(int);
static void _close(int);
static mapid_t _mmap(int fd, void * addr);
static void _munmap(mapid_t mapping);
static bool _chdir (const char *);
static bool _mkdir (const char *);
static bool _readdir (int, char *);
static bool _isdir (int);
static int _inumber (int);

static void * read_stack (void *, int);
static void check_valid_vaddr (const void *);

static struct lock exec_lock;
static struct lock filesys_lock;

/* read stack content given stack pointer ESP and offset OFFSET */
static void *
read_stack (void * esp, int offset)
{
  check_valid_vaddr(esp + offset); 
  return (uint32_t *)*(uint32_t *)(esp + offset);
}

/* check whether pointer vaddr is accesssing a valid memory */
static void
check_valid_vaddr (const void * vaddr)
{
  struct thread * cur = thread_current();

  if (is_kernel_vaddr(vaddr) || vaddr == NULL)
    _exit(-1);
}

void
syscall_init (void) 
{
  lock_init (&exec_lock);
  lock_init (&filesys_lock);
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *f) 
{

  check_valid_vaddr(f->esp);
  thread_current()->esp = f->esp;

  void * arg1, * arg2, * arg3;

  switch(* (int *) f->esp)
  {
    case SYS_HALT:
      _halt();
      break;
    case SYS_EXIT:
      arg1 = read_stack(f->esp, 4);
      _exit((int) arg1);
      break;
    case SYS_EXEC:
      arg1 = read_stack(f->esp, 4);
      f->eax = _exec((const char*) arg1);
      break;
    case SYS_WAIT:
      arg1 = read_stack(f->esp, 4);
      f->eax = _wait((int) arg1);
      break;
    case SYS_CREATE:
      arg1 = read_stack(f->esp, 4);
      arg2 = read_stack(f->esp, 8);
      f->eax = _create((const char*) arg1, (unsigned) arg2);
      break;
    case SYS_REMOVE:
      arg1 = read_stack(f->esp, 4);
      f->eax = _remove((const char *) arg1);
      break;
    case SYS_OPEN:
      arg1 = read_stack(f->esp, 4);
      f->eax = _open((const char*) arg1);
      break;
    case SYS_FILESIZE:
      arg1 = read_stack(f->esp, 4);
      f->eax = _filesize((int) arg1);
      break;
    case SYS_READ:
      arg1 = read_stack(f->esp, 4);
      arg2 = read_stack(f->esp, 8);
      arg3 = read_stack(f->esp, 12);
      f->eax = _read ((int) arg1, (void *) arg2, (unsigned) arg3);
      break;
    case SYS_WRITE:
      arg1 = read_stack(f->esp, 4);
      arg2 = read_stack(f->esp, 8);
      arg3 = read_stack(f->esp, 12);
      f->eax = _write ((int) arg1, (const void *) arg2, (unsigned) arg3);
      break;
    case SYS_SEEK:
      arg1 = read_stack(f->esp, 4);
      arg2 = read_stack(f->esp, 8);
      _seek((int) arg1, (unsigned) arg2);
      break;
    case SYS_TELL:
      arg1 = read_stack(f->esp, 4);
      f->eax = _tell((int) arg1);
      break;
    case SYS_CLOSE:
      arg1 = read_stack(f->esp, 4);
      _close ((int) arg1);
      break;
    case SYS_MMAP:
      arg1 = read_stack(f->esp, 4);
      arg2 = read_stack(f->esp, 8);
      f->eax = _mmap((int) arg1, (void *) arg2);
      break;
    case SYS_MUNMAP:
      arg1 = read_stack(f->esp, 4);
      _munmap((mapid_t) arg1);
      break;
    case SYS_CHDIR:
      arg1 = read_stack(f->esp, 4);
      f->eax = _chdir((const char *) arg1);
      break;
    case SYS_MKDIR:
      arg1 = read_stack(f->esp, 4);
      f->eax = _mkdir((const char *) arg1);
      break;
    case SYS_READDIR:
      arg1 = read_stack(f->esp, 4);
      arg2 = read_stack(f->esp, 8);
      f->eax = _readdir((int) arg1, (char *) arg2);
      break;
    case SYS_ISDIR:
      arg1 = read_stack(f->esp, 4);
      f->eax = _isdir((int) arg1);
      break;
    case SYS_INUMBER:
      arg1 = read_stack(f->esp, 4);
      f->eax = _inumber((int) arg1);
      break;
    default:
      _exit(-1);
      break;
  }
}

static void
_halt(void)
{
  shutdown_power_off ();
}

static void
_exit(int exit_status)
{
  struct thread * cur = thread_current();
  struct process_info * pi = cur->process_info;
  sema_down (&pi->exit_guard);
  if (!pi->exit_status_set)
  {
    pi->exit_status = exit_status;
    pi->exit_status_set = true;
    
    if (!cur->must_wait)
      printf("%s: exit(%d)\n", cur->name, exit_status);
    if (!cur->is_kernel && cur->nr_mmaps > 0)
    {
      int mmap_idx;
      for (mmap_idx = 0; mmap_idx < MMAP_LIST_SIZE && cur->nr_mmaps > 0; mmap_idx++)
      {
        if (list_size(&cur->mmaps_list[mmap_idx]) > 0)
        {
          _munmap(mmap_idx);
          continue;
        }
      }
    }
    file_close (cur->file);

    sema_up (&pi->exit_sema);
    sema_up (&pi->exit_guard);
    //printf("killing\n");
    thread_exit();
  }
}

void
syscall_exit (int exit_status)
{
  _exit (exit_status);
}

static pid_t
_exec(const char* cmd)
{
  check_valid_vaddr(cmd);
  lock_acquire (&exec_lock);
  int pid = process_execute (cmd);
  lock_release (&exec_lock);
  return pid;
}

static int
_wait(pid_t pid)
{
  int i = process_wait(pid);
  return i;
}

static bool
_create(const char* file, unsigned size)
{
  check_valid_vaddr(file);
  if (strlen (file) == 0 || strlen (file) > NAME_MAX)
    return false;

  lock_acquire (&g_filesys_lock);
  unsigned ret = filesys_create(file, size);
  lock_release (&g_filesys_lock);

  return ret;
}

static bool
_remove(const char* file)
{
  check_valid_vaddr(file);
  if ( strlen (file) == 0 )
    _exit(-1);
  
  lock_acquire (&g_filesys_lock);
  bool ret = filesys_remove (file);
  lock_release (&g_filesys_lock);

  return ret;
}

static int
_open(const char* file)
{
  struct thread * cur = thread_current();

  check_valid_vaddr(file);

  if (!strlen(file))
    return -1;

  int fd;

  for (fd = 2; fd < MAX_FILES; fd++)
  {
    if (cur->openfiles[fd] == NULL)
    {
      lock_acquire (&g_filesys_lock);
      cur->openfiles[fd] = (void *) filesys_open(file);
      lock_release (&g_filesys_lock);
      if (cur->openfiles[fd] != NULL)
      {
        return fd;
      }
    }
  }
  return -1;
}

static int
_filesize(int fd)
{
  struct thread * t = thread_current ();
  if (t->openfiles[fd] != NULL)
  {
    lock_acquire (&g_filesys_lock);
    int ret = file_length(t->openfiles[fd]);
    lock_release (&g_filesys_lock);
    return ret;
  }
  return -1;
}

static int
_read(int fd, void* buffer, unsigned size)
{
  unsigned ret = -1;
  struct thread * t;

  check_valid_vaddr(buffer);

  if (fd < 0 || fd == 1 || fd > MAX_FILES)
    goto done;

  if (fd == STDIN_FILENO)
  {
    char c;
    char * c_ptr = (char *) buffer;
    ret = 0;
    while (ret < size)
    {
      c = input_getc();
      c_ptr[ret++] = c;
      if (c == '\0')
        goto done;
    }
  }
  else
  {
    t = thread_current ();

    if (t->openfiles[fd] == NULL)
      goto done;

    ret = file_read (t->openfiles[fd], buffer, size);
  }

done:
  return ret;
}

static int
_write(int fd, const void* buffer, unsigned size)
{
  unsigned ret = -1;

  check_valid_vaddr(buffer);

  if (fd < 0 || fd == 0 || fd > MAX_FILES)
    goto done;

  if (fd == STDOUT_FILENO)
  {
    for (ret = 0; ret < size && *(char *)(buffer + ret) != '\0'; ret++)
      putchar(*(char *)(buffer + ret));
  }
  else
  {
    struct thread * t = thread_current ();

    if (t->openfiles[fd] == NULL)
      goto done;

    lock_acquire (&g_filesys_lock);
    ret = file_write (t->openfiles[fd], buffer, size);
    lock_release (&g_filesys_lock);
  }

done:
  return ret;
}

static void _seek(int fd, unsigned pos)
{
  if (fd < 2 || fd > MAX_FILES)
    _exit(-1);

  struct thread * cur = thread_current();

  if (cur->openfiles[fd] == NULL)
    _exit(-1);

  file_seek (cur->openfiles[fd], pos);
}

static unsigned _tell(int fd)
{
  if (fd < 2 || fd > MAX_FILES)
    _exit(-1);

  struct thread * cur = thread_current();

  if (cur->openfiles[fd] == NULL)
    _exit(-1);

  return file_tell (cur->openfiles[fd]);
}

static void
_close(int fd)
{
  if (fd >= 2 && fd <= MAX_FILES)
  {
    struct thread * cur = thread_current();

    if (cur->openfiles[fd] == NULL)
    {
      /* do nothing */
    }
    else
    {
      lock_acquire (&g_filesys_lock);
      file_close (cur->openfiles[fd]);
      cur->openfiles[fd] = NULL;
      lock_release (&g_filesys_lock);
    }
  }
}

static mapid_t
_mmap(int fd, void * addr)
{
  if (fd < 2 || fd > MAX_FILES || addr == 0)
    return -1;

  if ((int) addr & (PGSIZE -1)) // check whether on page boundary
    return -1;
  if (addr > (PHYS_BASE - ABSOLUTE_STACK_SIZE * PGSIZE))
    return -1;

  struct thread * t = thread_current();

  if (!t->mmap_initialized)
  {
    int i;
    for (i = 0; i < MMAP_LIST_SIZE; i++)
      list_init (&t->mmaps_list[i]);
    t->mmap_initialized = true;
  }

  off_t f_len = file_length (t->openfiles[fd]);

  if (t->openfiles[fd] == NULL)
    return -1;

  if (f_len == 0)
    _exit(-1);

  /* choose a free mmap slot */
  int mmap_idx;
  for (mmap_idx = 0; mmap_idx < MMAP_LIST_SIZE; mmap_idx++)
    if (list_size(&t->mmaps_list[mmap_idx]) == 0)
      break;
  
  if (mmap_idx == MMAP_LIST_SIZE)
    return -1;

  t->nr_mmaps++;
  /* set up supplementary page tables for this mapping */
  off_t offset = 0;
  uint32_t * pte;
  struct page * p;
  while (f_len > 0)
  {
    pte = page_get_pte (t->pagedir, pg_round_down(addr), true);
    p = page_lookup (pte);

    if (p != NULL) // check whether page is already in use
      return -1;

    p = init_page ();
    p->flags |= PAGE_is_mmap;
    p->inode = file_get_inode(t->openfiles[fd]);
    inode_reopen (p->inode);  // 
    p->offset = offset;
    p->sector = byte_to_sector_pub (p->inode, p->offset);

    if (f_len > PGSIZE)
      p->read_bytes = PGSIZE;
    else
      p->read_bytes = f_len;
    p->writable = true;
    p->owner = t;
    p->pte = pte;
    p->mmap_addr = addr;
    page_save (p);
    
    list_push_back (&t->mmaps_list[mmap_idx], &p->mmap_list_elem);

    /* advance */
    f_len -= PGSIZE;
    addr += PGSIZE;
    offset += PGSIZE;
  }

  return mmap_idx;
}

static void
_munmap(mapid_t mapping)
{
  if (mapping > MMAP_LIST_SIZE || mapping < 0) 
    _exit (-1);

  struct thread * t = thread_current();  

  if (list_size(&t->mmaps_list[mapping]) == 0)
    return;

  /* use mmaps_list to traverse all pages mapped to the file, write-back
    to file is dirty */
  struct list_elem * e; 
  struct page * p;
  bool is_last = false;
  while (list_size(&t->mmaps_list[mapping]) > 0)
  {
    e = list_begin (&t->mmaps_list[mapping]);
    if ( e == list_end(&t->mmaps_list[mapping]) ||
        list_size(&t->mmaps_list[mapping]) == 1)
      is_last = true;
    p = list_entry (e, struct page, mmap_list_elem);
    list_remove (e);

    if (*p->pte & PTE_D)
      inode_write_at (p->inode, p->fs->paddr, p->read_bytes, p->offset);

    t->nr_mmaps--;
    p->flags |= PAGE_unmmapped;
    if (is_last)
    {
      return;
    }
  }
}

static bool
_chdir (const char * name)
{
  struct thread * t = thread_current ();
  struct dir * dir = dir_get_pwd ();
  struct inode * inode;
  if (dir_lookup (dir, name, &inode))
  {
    if (inode == NULL)
      return false;
    dir_close (t->pwd);
    t->pwd = dir_open (inode);
    return true;
  }
  return false;
}

static bool
_mkdir (const char * name)
{
  if (!strlen (name))
    return false;

  return dir_mkdir (name);
}

static bool
_readdir (int fd, char * name)
{
  struct thread * t = thread_current();
  if (t->openfiles[fd] == NULL)
    return false;
  if (!_isdir(fd))
    return false;
  return dir_readdir (t->openfiles[fd], name);
}

static bool
_isdir (int fd)
{
  struct thread * t = thread_current();
  if (t->openfiles[fd] == NULL)
    return false;
  return inode_get_type (file_get_inode(t->openfiles[fd])) == DIRECTORY;
}

static int
_inumber (int fd)
{
  struct thread * t = thread_current();
  if (t->openfiles[fd] == NULL)
    return -1;
  return inode_get_inumber (file_get_inode(t->openfiles[fd]));
}
